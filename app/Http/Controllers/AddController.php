<?php

namespace App\Http\Controllers;

use Illuminate\Database;
use Illuminate\Http\Request;
use App\Models\add;
use Auth;
class AddController extends Controller
{


    function inserter(Request $req){
        $add = new add;
        $username = Auth::user()->name;
        $add -> title=$req->title;
        $add -> stitle=$req->stitle;
        $add -> text=$req->text;
        $add -> rev=$req->rev;
        $add -> name=$username;

        //saglabās atēlu norādītajā vailā ( $file->move('uploads/image',$filename);) kā arī saglabās datubāzē nosaukumu
        if($req -> hasfile('img')){
            $file=$req->file('img');
            $extention = $file->getClientOriginalExtension();
            $filename = time().'.'.$extention;
            $file->move('uploads/image',$filename);
            $add->img = $filename;
        }
        
        $add -> save();
        return redirect()->back();

    }
        

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class add extends Model
{
    protected $table = 'add';
    use HasFactory;
    public $timestamps=false;
    protected $fillable = [
        'title',
        'stitle',
        'text',
        'rev',
        'img',
    ];
}
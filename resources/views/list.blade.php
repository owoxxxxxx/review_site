<!DOCTYPE html>
<html>
  <head>
    <title>( ͡° ͜ʖ ͡°)</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/scrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fot.css') }}">
    <link rel="stylesheet" href="{{ asset('css/display.css') }}">
    <link rel="stylesheet" href="{{ asset('css/size.css') }}">
    <link rel="stylesheet" href="{{ asset('css/scss.scss') }}">
    
  </head>
  <body>


<!--nav-->
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="a nav-link" href="http://127.0.0.1:8000/">Pr!x incorporated</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item mx-2">
              <a class="a nav-link" href="#">Reviews</a>
            </li>
            <li class="nav-item mx-2">
              <a class="a nav-link" href="#"></a>
            </li>    
          </ul>
        </div>
      </nav>
<!--nav-->



        



    <div class="cborder">
        <p class="stattitle"><b>{{$data->title}}: Review</b></p>  
        <div class="statunder">
            <img class="statimg" src="{{asset('uploads/image/'. $data->img)}}" alt="hmm">
            <p class="statncolor my-3">By {{$data->name}}</p>
        </div>
        <div class="statline my-3"></div>
            <div class="stattext">
            <p>{{$data->stitle}}</p>
        </div>

        <div class="mt-4"></div>
            <p class="stattitlesub"><b>Best of {{$data->title}}</b></p>
            <div class="statcline mb-4"></div>
            <div class="stattext">
                <div class="d-flex justify-content-center">
                <p>{{$data->text}}</p>
            </div>
        </div>


  
<!-- izvada no db un js -->
<div class="sdo"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm">
          <b class="my-4 scolorer ssizer">Final verdict: </b>
          <b class="my-4 scolorer ssizer" id="test"></b>
        </div>
        <div class="col-sm">
          <div id="hexagon" class="text-dark d-flex justify-content-center align-items-center">
            <b class="hexa">{{$data->rev}}</b>
          </div>
        </div>
      </div>
    </div>
<!-- izvada no db un js -->


    
    </div>
      <div id="fot">

        <div class="container">
          <div class="row">
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">yes</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">stuff</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">ABOUT US</a></p></div>
            <div class="w-100"></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">no</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">text</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">CAREERS</a></p></div>
            <div class="w-100"></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">maybe</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">i dont know</a></p></div>
            <div class="col"><p class="textd"><a href="legal.html" class="fbuttons">PRIVACY & LEGAL</a></p></div>
            <div class="w-100"></div>
            <div class="col"></div>
            <div class="col col-md-2"><p id="smf">© 2021 No Rights Reserved. :(</p></div>
          </div>
        </div>

      </div>



  </body>


  <script>

    var num = {{$data->rev}};
    if(num == 10){
      document.getElementById("test").innerHTML = "Truly magnificent experience";
    }
    else if(num == 9){
      document.getElementById("test").innerHTML = "Amazing";
    }
    else if(num == 8){
      document.getElementById("test").innerHTML = "Great";
    }
    else if(num == 7){
      document.getElementById("test").innerHTML = "Good";
    }
    else if(num == 6){
      document.getElementById("test").innerHTML = "Decent";
    }
    else if(num == 5){
      document.getElementById("test").innerHTML = "Depends on your standards";
    }
    else if(num == 4){
      document.getElementById("test").innerHTML = "Bad";
    }
    else if(num == 3){
      document.getElementById("test").innerHTML = "Horrible";
    }
    else if(num == 2){
      document.getElementById("test").innerHTML = "Why would you even touch this";
    }
    else if(num == 1){
      document.getElementById("test").innerHTML = "Just god awful";
    }
    else{
      document.getElementById("test").innerHTML = "This is truly mind bending you have managed to exceed the intended values";
    }






</script>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>
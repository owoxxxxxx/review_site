<!DOCTYPE html>
<html>
  <head>
    <title>( ͡° ͜ʖ ͡°)</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="css/scrollbar.css">
    <link rel="stylesheet" href="css/fot.css">
    <link rel="stylesheet" href="css/display.css">
    <link rel="stylesheet" href="css/size.css">
</head>
<body>
<div class="msp"></div>
<div class="d-flex justify-content-center ">
    <form action="{{url('add')}}" class="needs-validation" method="POST" enctype="multipart/form-data">
        @csrf
        <div id="largediv"></div>
            <input class="form-control d-inline" type="text" name="title" placeholder="title" maxlength="60" required="true">
                <br><br>
            <input class="form-control d-inline" type="text" name="stitle" placeholder="sub title" maxlength="90" required="true">
                <br><br>
                
            <div class="input-group">
                <div class="input-group-prepend">
                </div>
                <textarea required="true" type="text" name="text" class="form-control largebox" aria-label="With textarea izsk"></textarea>
            </div>

            
<div class="my-4"></div>

<div class="range-wrap">
  <input type="range" name="rev" class="range" min="1" max="10">
  <output class="bubble"></output>
</div>

<input class="form-control d-inline" type="file" name="img" placeholder="????" maxlength="200" required="true">
<p class="rez">Recommended resolution 1920 × 1200</p>
<br><br>



<div class="d-flex justify-content-center">
    <button type="submit" class="insertbutton">Save</button>


</form>


</div>
</div>







<!--_____________________________________________-->


</body>

  <script src="js/js.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>

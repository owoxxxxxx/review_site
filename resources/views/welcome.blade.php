<!DOCTYPE html>
<html>
  <head>
    <title>( ͡° ͜ʖ ͡°)</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="css/scrollbar.css">
    <link rel="stylesheet" href="css/fot.css">
    <link rel="stylesheet" href="css/display.css">
    <link rel="stylesheet" href="css/size.css">
    
  </head>
  <body>


      <!--nav-->
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="a nav-link" href="#">Pr!x incorporated</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item mx-2">
              <a class="a nav-link" href="#">Reviews</a>
            </li>
            <li class="nav-item mx-2">
              <a class="a nav-link" href="#"></a>
            </li>    
          </ul>
        </div>
      </nav>

      <!-- jāsataisa lai būtu precīzi vidū un samazinot arī   -->
      <div class="myc"></div>
      <div id="showc">
        <div id="center">
            <a href="http://127.0.0.1:8000/newworld"><img src="photos/test.jpg" alt="hmm" class="spacer"></a>
            <a href="http://127.0.0.1:8000/forzahorizon "><img src="photos/fftest.jpg" alt="hmm" class="spacer"></a>
            <a href="http://127.0.0.1:8000/hades"><img src="photos/htest.jpg" alt="hmm" class="spacer"></a>
            <a href="http://127.0.0.1:8000/tombraider"><img src="photos/ttest.jpg" alt="hmm" class="spacer"></a>
            
        </div>
      </div>



      <!-- pec top rev vis uz leju kas iet-->

      <!-- carouselt-->
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="photos/hades.jpg" alt="slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="photos/tomb.jpg" alt="slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="photos/new world.jpg" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <!-- carouselt-->

      <!-- jāatcerās kad izmantos db kad vaidzēs tikai vienu line jo automātiski vel tiks iedoti-->
      <div class="box">

      @foreach($owo as $owo)
        <div class="row">
          <div class="col"><a href="{{"list/".$owo['ID']}}"><img src="{{asset('uploads/image/'. $owo->img)}}" alt="hmm" class="nspecial"></a></div>
          <div class="col-sm-8 my-auto" id="yews">
            <p id="titletext">{{$owo['title']}}</p>
            <p>{{$owo['stitle']}} </p>
          </div>
        </div>

        
        <div class="line"></div>

        
        @endforeach

      
            
           
        <!-- atstāt to jo ja ir php beigsies ar līniju un iskatīsies labāk-->

        <div class="row">
          <div class="col"><a class="smallbtext">More reviews</a></div>
          <div class="col-sm-9 my-auto"><!-- varbū varētu pielikt kautko vēl lai būtu vairāk--></div>
        </div>
        <!--beigas-->

  


      </div>

      <div id="fot">

        <div class="container">
          <div class="row">
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">yes</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">stuff</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">ABOUT US</a></p></div>
            <div class="w-100"></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">no</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">text</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">CAREERS</a></p></div>
            <div class="w-100"></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">maybe</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">i dont know</a></p></div>
            <div class="col"><p class="textd"><a href="legal.html" class="fbuttons">PRIVACY & LEGAL</a></p></div>
            <div class="w-100"></div>
            <div class="col"></div>
            <div class="col col-md-2"><p id="smf">© 2021 No Rights Reserved. :(</p></div>
          </div>
        </div>

      </div>
  </body>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>


<!-- https://stackoverflow.com/questions/67538206/laravel-8-how-to-restrict-access-of-html-content-by-using-laravel-gates -->
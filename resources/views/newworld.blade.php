<!DOCTYPE html>
<html>
  <head>
    <title>( ͡° ͜ʖ ͡°)</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="css/scrollbar.css">
    <link rel="stylesheet" href="css/fot.css">
    <link rel="stylesheet" href="css/display.css">
    <link rel="stylesheet" href="css/size.css">
    
  </head>
  <body>


<!--nav-->
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="a nav-link" href="http://127.0.0.1:8000/">Pr!x incorporated</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item mx-2">
              <a class="a nav-link" href="#">Reviews</a>
            </li>
            <li class="nav-item mx-2">
              <a class="a nav-link" href="#"></a>
            </li>    
          </ul>
        </div>
      </nav>
<!--nav-->

    <div class="cborder">
        <a href="https://store.steampowered.com/app/1145360/Hades/" target="_blank" class="statcon" >New world</a>
        <p class="stattitle"><b>New world: Review</b></p>  
        <div class="statunder">
        <img class="statimg" src="photos/new world.jpg" alt="hmm">
            <p class="statncolor my-3">By Pr!x</p>
        </div>
        <div class="statline my-3"></div>
        <div class="stattext">
            <p>In the ancient Greek myth of Sisyphus, a poor soul is doomed to forever push a boulder uphill without rest. Each time he’d push it up, it would roll back down again and he’d have to start anew. But what if that Herculean climb uphill was actually awesome combat against a randomized variety of enemies as you fight your way out of Hell, with a wide range of godly powers that grow and combine in interesting ways as you progress? And what if, instead of going back to zero, you got a little stronger each time with some help from a colorful cast of allies and enemies who remember each of your attempts? That’s Hades’ premise, and the Greek mythology-themed rogue-lite that developer Supergiant has built around it makes going to Hell a joy.</p>
        </div>

        <div class="mt-4"></div>
        <p class="stattitlesub"><b>Inevitably, the forces of Hades will overwhelm you.</p></b>
        <div class="statcline mb-4"></div>
        <div class="stattext">
            <div class="d-flex justify-content-center">
                <p>The final mechanic in Zag’s moveset is the cast: a skill-shot based projectile that fires a red jewel into an enemy for a quick burst of damage. The jewel embeds itself into a target, disqualifying you from firing another cast until you kill the enemy and retrieve your ammunition, so shooting it at the biggest, meanest enemy in the room isn’t always the best idea. As with each one of Zag's moves, using it well involves a delicate balance between risk and reward.Almost inevitably, though, the forces of Hades will overwhelm you eventually, and every death sends Zag back home to start over from scratch, save for persistent currencies and progress in relationships with the wonderful cast of characters who inhabit the Underworld, from lord Hades himself all the way down. So you’re going to be spending a lot of time getting to know everyone -- and it’s time very well spent.But the heart and soul of Hades, outside of its combat, lies in Zag’s interaction with various deities and mythic figures from Greek myth, like Achilles, Orpheus, and more. It turns out that adding a dash of dating simulator mechanics to a rogue-lite was the secret sauce in making good use of a large cast of interesting characters. Each personality feels like an authentic reinterpretation of a classic Greek myth, and they’re all a joy to behold. For instance, instead of painting Sisyphus as a tragic character, he's an optimist who you encounter on a break while the gods aren’t watching. Even his boulder (affectionately known as Bouldy) sports a carved smile that reacts in silence upon interaction. Investing in Zag’s relationships with each character paves the way for interesting backstory reveals, world-building, new sidequests, or even items to assist on your future escape attempts.Not only are conversations and side stories actively engaging -- attempting to reunite Achilles with his life-long partner, Patroclus, or Orpheus with his long-lost muse Euridice, are genuinely moving and hearken back to the kind of melodrama the ancient Greeks were so good at -- there are also gameplay benefits! Advancing these relationships far enough grants you persistent keepsakes and companions, which are both invaluable tools in aiding Zag's quest to run away from home. Of course, not all keepsakes are as useful as others; given the choice between the Lucky Tooth that grants you an additional chance at life if you’re killed or Olympic keepsakes that increase your chance at better drops, I’m going with a second life every time.</p>
            </div>
        </div>
    </div>
    



  


      </div>

      <div id="fot">

        <div class="container">
          <div class="row">
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">yes</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">stuff</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">ABOUT US</a></p></div>
            <div class="w-100"></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">no</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">text</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">CAREERS</a></p></div>
            <div class="w-100"></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">maybe</a></p></div>
            <div class="col"><p class="textd"><a href="legal" class="fbuttons">i dont know</a></p></div>
            <div class="col"><p class="textd"><a href="legal.html" class="fbuttons">PRIVACY & LEGAL</a></p></div>
            <div class="w-100"></div>
            <div class="col"></div>
            <div class="col col-md-2"><p id="smf">© 2021 No Rights Reserved. :(</p></div>
          </div>
        </div>

      </div>
  </body>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>
